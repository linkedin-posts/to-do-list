import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_list/input_dialog.dart';
import 'package:to_do_list/single_task_box.dart';
import 'package:to_do_list/task.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Color(int.parse("0xFF02A7B1")),
        elevation: 10,
        shadowColor: Colors.grey,
        title: Text("To-Do List",
            style: GoogleFonts.quicksand(
              fontSize: 32,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            )),
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: ListView.builder(
            itemCount: taskList.length,
            itemBuilder: (context, index) {
              return SingleTaskBox(
                refreshList: refreshList,
                index: index,
                task: taskList[index],
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: openInputDialog,
          backgroundColor: Color(int.parse("0xFF008B94")),
          shape: const CircleBorder(),
          child: const Icon(
            Icons.add,
            color: Colors.white,
            size: 40,
          )),
    );
  }

  void openInputDialog() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return InputDialog(refreshList);
        });
  }

  void refreshList() {
    setState(() {});
  }
}
