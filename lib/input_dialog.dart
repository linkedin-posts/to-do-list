import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:to_do_list/task.dart';

// ignore: must_be_immutable
class InputDialog extends StatefulWidget {
  VoidCallback refreshList;
  InputDialog(this.refreshList, {super.key});

  @override
  State<InputDialog> createState() => _InputDialogState();
}

class _InputDialogState extends State<InputDialog> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  bool isTitleError = false;
  bool isDescriptioError = false;
  bool isDateError = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontSize: 26,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "Title",
              style: _getTextStyle(),
            ),
            SizedBox(
              height: 50,
              child: TextField(
                controller: titleController,
                keyboardType: TextInputType.text,
                onTap: () {
                  setState(() {
                    isTitleError = false;
                  });
                },
                decoration: InputDecoration(
                    errorText: isTitleError ? "Enter the Text" : null,
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 1)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1))),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "Description",
              style: _getTextStyle(),
            ),
            SizedBox(
              height: 100,
              child: TextField(
                controller: descriptionController,
                onTap: () {
                  setState(() {
                    isDescriptioError = false;
                  });
                },
                decoration: InputDecoration(
                    errorText: isDescriptioError ? "Enter the Text" : null,
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 1)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1))),
                maxLines: 3,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "Date",
              style: _getTextStyle(),
            ),
            SizedBox(
              height: 50,
              child: TextField(
                controller: dateController,
                onTap: setDate,
                readOnly: true,
                decoration: InputDecoration(
                    errorText: isDateError ? "Please Select the Date" : null,
                    suffixIcon: Icon(
                      Icons.calendar_month_outlined,
                      color: Color(int.parse("0xFF008B94")),
                    ),
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide:
                            const BorderSide(color: Colors.red, width: 1)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                            color: Color(int.parse("0xFF008B94")), width: 1))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                    onPressed: checkValidations,
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Color(int.parse("0xFF008B94")),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                    child: Text(
                      "Submit",
                      style: GoogleFonts.inter(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  TextStyle _getTextStyle() {
    return GoogleFonts.quicksand(
        fontSize: 18,
        color: Color(int.parse("0xFF008B94")),
        fontWeight: FontWeight.w400);
  }

  void checkValidations() {
    if (titleController.text.isEmpty) {
      setState(() {
        isTitleError = true;
      });
    } else if (descriptionController.text.isEmpty) {
      setState(() {
        isDescriptioError = true;
      });
    } else if (dateController.text.isEmpty) {
      setState(() {
        isDateError = true;
      });
    } else {
      addTask();
      widget.refreshList();
      updateList();
      Navigator.of(context).pop();
    }
  }

  void addTask() {
    Task newTask = Task(
        taskName: titleController.text,
        description: descriptionController.text,
        date: dateController.text,
        isCompleted: false);

    taskList.add(newTask);

    titleController.clear();
    descriptionController.clear();
    dateController.clear();
  }

  void setDate() async {
    setState(() {
      isDateError = false;
    });
    DateTime? datetime = await showDatePicker(
        context: context,
        firstDate: DateTime(2024),
        lastDate: DateTime(2025),
        initialDate: DateTime.now());

    if (datetime != null) {
      setState(() {
        dateController.text = DateFormat.yMMMd().format(datetime);
      });
    }
  }
}
