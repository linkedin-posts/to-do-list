import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:to_do_list/edit_dialog.dart';
import 'package:to_do_list/task.dart';

// ignore: must_be_immutable
class SingleTaskBox extends StatefulWidget {
  int index;
  Task task;
  VoidCallback refreshList;
  SingleTaskBox(
      {required this.refreshList,
      required this.task,
      required this.index,
      super.key});

  @override
  State<SingleTaskBox> createState() => _SingleTaskBoxState();
}

class _SingleTaskBoxState extends State<SingleTaskBox> {
  final List<Color> colorsList = [
    Color(int.parse("0xFFFAE8E8")),
    Color(int.parse("0xFFE8EDFA")),
    Color(int.parse("0xFFFAF9E8")),
    Color(int.parse("0xFFFAE8FA")),
  ];

  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: GestureDetector(
        onTap: showDetailedDialog,
        child: Container(
          height: 135,
          width: double.infinity,
          decoration: BoxDecoration(
              color: colorsList[widget.index % 4],
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Colors.grey[300]!,
                  offset: const Offset(-1, -1),
                  spreadRadius: 5,
                ),
                BoxShadow(
                  blurRadius: 8,
                  color: Colors.grey[300]!,
                  offset: const Offset(1, 1),
                  spreadRadius: 5,
                ),
              ]),
          alignment: Alignment.center,
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              Expanded(
                child: Row(
                  children: [
                    SizedBox(
                      width: 60,
                      child: Transform.scale(
                        scale: 1.5,
                        child: Checkbox(
                          fillColor: MaterialStatePropertyAll(
                              Color(int.parse("0xFF02A7B1"))),
                          side: const BorderSide(
                              color: Colors.black, style: BorderStyle.solid),
                          onChanged: (value) {
                            setState(() {
                              widget.task.isCompleted = value!;
                            });
                            updateList();
                          },
                          value: widget.task.isCompleted,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        flex: 4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.task.taskName,
                              style: GoogleFonts.quicksand(
                                  color: Color(int.parse("0xFF000000")),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  decoration: widget.task.isCompleted
                                      ? TextDecoration.lineThrough
                                      : TextDecoration.none),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              widget.task.description,
                              style: Theme.of(context).textTheme.displaySmall,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                            ),
                          ],
                        )),
                  ],
                ),
              ),
              Row(
                children: [
                  Text(
                    widget.task.date,
                    style: GoogleFonts.quicksand(
                        color: Color(int.parse("0xFF848484"))),
                  ),
                  const Spacer(),
                  GestureDetector(
                    onTap: showEditDialog,
                    child: Icon(
                      Icons.edit_outlined,
                      size: 28,
                      color: Color(int.parse("0xFF008B94")),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    onTap: deleteThisTask,
                    child: Icon(
                      Icons.delete_outline,
                      size: 28,
                      color: Color(int.parse("0xFF008B94")),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void showEditDialog() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return EditDialog(
            refreshList: widget.refreshList,
            index: widget.index,
          );
        });
  }

  void showDetailedDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colorsList[widget.index % 4],
            title: Text(widget.task.taskName),
            titleTextStyle: GoogleFonts.quicksand(
                fontWeight: FontWeight.w700, color: Colors.black, fontSize: 24),
            actions: [
              Text(
                widget.task.date,
                style: GoogleFonts.quicksand(
                    color: Color(int.parse("0xFF848484")),
                    fontWeight: FontWeight.w500),
              )
            ],
            content: Text(
              widget.task.description,
              style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 16),
            ),
          );
        });
  }

  void deleteThisTask() async {
    bool delete = false;

    await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: colorsList[widget.index % 4],
            content: SizedBox(
              height: 120,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "Do you want to Delete this Task ?",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.quicksand(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        onPressed: () {
                          delete = true;
                          Navigator.of(context).pop();
                        },
                        style: TextButton.styleFrom(
                            minimumSize: const Size(100, 40),
                            backgroundColor: Color(int.parse("0xFF008B94")),
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        child: Text(
                          "Yes",
                          style: GoogleFonts.inter(
                              fontSize: 16, color: Colors.white),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        style: TextButton.styleFrom(
                            minimumSize: const Size(100, 40),
                            backgroundColor: Color(int.parse("0xFF008B94")),
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        child: Text(
                          "Cancel",
                          style: GoogleFonts.inter(
                              fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });

    if (delete) {
      taskList.removeAt(widget.index);
      widget.refreshList();
      updateList();
    }
  }
}
