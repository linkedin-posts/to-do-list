import 'dart:developer';

import 'package:hive/hive.dart';

class Task {
  String taskName;
  String description;
  String date;
  bool isCompleted;

  Task(
      {required this.taskName,
      required this.description,
      required this.date,
      required this.isCompleted});
}

List<Task> taskList = [
  Task(
    taskName: "Welcome to YourToDoApp!",
    description: "Start using YourToDoApp and stay organized.",
    date: "Mar 1, 2024",
    isCompleted: false,
  ),
  Task(
    taskName: "Create a New Task",
    description:
        "Click the '+' button to add a new task to your list. You can add Titile , Description , Date",
    date: "Mar 1, 2024",
    isCompleted: false,
  ),
  Task(
    taskName: "Mark Task as Completed",
    description: "Tap on a Check Box to mark it as completed.",
    date: "Mar 1, 2024",
    isCompleted: false,
  ),
];

Future setTaskList() async {
  var box = await Hive.openBox('taskList');

  var demo = box.get('taskList');

  if (demo == null) {
    List tempList = List.generate(taskList.length, (index) {
      return [
        taskList[index].taskName,
        taskList[index].description,
        taskList[index].date,
        taskList[index].isCompleted,
      ];
    });
    await box.put('taskList', tempList);
    log("List Saved Sucessfully");
  } else {
    List tempList = box.get('taskList');
    taskList = List.generate(
        tempList.length,
        (index) => Task(
            taskName: tempList[index][0],
            description: tempList[index][1],
            date: tempList[index][2],
            isCompleted: tempList[index][3]));

    log("List Retrived Sucessfully");
  }
}

Future updateList() async {
  var box = await Hive.openBox('taskList');

  List tempList = List.generate(taskList.length, (index) {
    return [
      taskList[index].taskName,
      taskList[index].description,
      taskList[index].date,
      taskList[index].isCompleted,
    ];
  });
  await box.put('taskList', tempList);
  log("List Updated Sucessfully");
}
